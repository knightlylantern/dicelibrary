﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[Serializable]
public class CrestDiceSide : DiceSide<Crest>
{
}


[CreateAssetMenu(menuName = "Dice/Crest Dice")]
public class CrestDice : Dice<CrestDiceSide>
{

}
