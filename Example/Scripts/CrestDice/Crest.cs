﻿using System;
using System.Collections.Generic;
using UnityEngine;

public  enum Crest
{
    [Crest(Image = "Crests/crest_attack")]
    ATTACK,
    [Crest(Image = "Crests/crest_defense")]
    DEFENSE,
    [Crest(Image = "Crests/crest_skill")]
    SKILL
}


public class CrestEnumInfo
{
    private static Type type = typeof(Crest);
    private static Dictionary<Enum, Sprite> images = new Dictionary<Enum, Sprite>();                                         

    public static Sprite GetImage(Enum value)
    {
        if (!images.ContainsKey(value)) {

            var attr = GetAttributes(Enum.GetName(type, value));
            if (attr == null) return null;
            images[value] = (Sprite)PrefabFactory.Load(attr.Image, typeof(Sprite));
        }

        return images[value];
    }


    public static CrestAttribute GetAttributes(string name)
    {
        if (name == null) return null;
        var field = type.GetField(name);
        if (field == null) return null;

        return Attribute.GetCustomAttribute(field, typeof(CrestAttribute)) as CrestAttribute;
    }
}

public class CrestAttribute : Attribute
{
    public string Image { get; set; }
}