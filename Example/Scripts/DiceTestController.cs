﻿
using RSG;
using UnityEngine;

public class DiceTestController : MonoBehaviour
{
    public Dice dice;
    public CrestDice diceCrest;
    public DiceAdvanced diceAdvanced;


    private void Awake()
    {
        if(dice!= null)
        new DiceDecorator<int>(dice, new Vector3(10, 0, 0)).Roll().Then(side =>
                {
                    Debug.Log(side);
                    return Promise.Resolved();
                });

        if (diceCrest != null)
            new DiceWithSidesDecorator<CrestDiceSide, Crest>(diceCrest, new Vector3(-10, 0, 0)).RollValue().Then(side =>
        {
            Debug.Log(side);
            return Promise.Resolved();
        });

        if (diceAdvanced != null)

            new DiceAdvancedDecorator(diceAdvanced, new Vector3(10, 0, 0)).Roll().Then(side =>
        {
            Debug.Log(side);
            return Promise.Resolved();
        });
    }




   
}

