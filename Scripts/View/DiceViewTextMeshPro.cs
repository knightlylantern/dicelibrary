﻿
using TMPro;
using UnityEngine;

public class DiceViewTextMeshPro : DiceView
{

    public TextMeshPro text;

    protected override void Initialize()
    {
        base.Initialize();
        text.gameObject.SetActive(false);
    }

    protected override void OnStartRoll()
    {
        base.OnStartRoll();
        text.gameObject.SetActive(false);
    }

    public override void ShowResult(object result)
    {
        text.text = result.ToString();
        text.gameObject.SetActive(true);
    }
}