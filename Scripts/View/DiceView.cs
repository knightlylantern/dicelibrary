﻿using System;
using RSG;
using UnityEngine;

public abstract class DiceView : MonoBehaviour
{
    Promise rollingPromise;
    Animator anim;


    void Awake()
    {
        anim = GetComponent<Animator>();
        Initialize();
    }

    public Promise RollView()
    {
        rollingPromise = new Promise();
        gameObject.SetActive(true);
        anim.SetTrigger("roll");

        OnStartRoll();
        return rollingPromise;
    }

    protected virtual void OnStartRoll()
    {
    }

    protected virtual void Initialize()
    {
    }

    private void LateUpdate()
    {
        if (rollingPromise != null)
        {

            if (!anim.GetCurrentAnimatorStateInfo(0).IsTag("Rolling") && !anim.IsInTransition(0))
            {
                rollingPromise.Resolve();
                rollingPromise = null;
            }
        }
    }

   

    public abstract void ShowResult(object result);
}

