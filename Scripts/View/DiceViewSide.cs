﻿
using UnityEngine;

public class DiceViewSide : DiceView
{

    public SpriteRenderer spriteRenderer;

    protected override void Initialize()
    {
        base.Initialize();
        spriteRenderer.gameObject.SetActive(false);
    }

    protected override void OnStartRoll()
    {
        base.OnStartRoll();
        spriteRenderer.gameObject.SetActive(false);
    }


    public override void ShowResult(object result)
    {
        spriteRenderer.gameObject.SetActive(true);

        spriteRenderer.sprite = ((ImageDiceSide)result).image;
    }
}