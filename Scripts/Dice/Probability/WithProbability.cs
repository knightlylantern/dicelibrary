﻿public interface WithProbability
{
    float Probability { get; }
}