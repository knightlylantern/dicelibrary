﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class DiceWithProbability<T> : IDice<T> where T : WithProbability
{
    public List<T> sides;

    public override T GetResult()
    {
        float totalProb = sides.Sum(p => p.Probability);
        float prob = UnityEngine.Random.value;

        var side = sides.First(s =>
        {
            var sideProb = s.Probability / totalProb;

            if (prob < sideProb)
                return true;
            prob -= sideProb;
            return false;
        });

        return side;
    }
}

