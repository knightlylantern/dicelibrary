﻿using System;
using UnityEngine;

[Serializable]
public class DiceSide<E> : ImageDiceSide, WithProbability
{
    public E value;
    public float prob = 1;

    public float Probability
    {
        get { return prob; }

    }
}

