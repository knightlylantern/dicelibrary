﻿using UnityEngine;

[CreateAssetMenu(menuName ="Dice/Advanced/Side/Basic Side")]
public class DiceSideAdvanced : ScriptableObject, WithProbability, IDiceSide
{
    public string value;

    [Range(0,1)]
    public float probability = 1;

    public float Probability => probability;

    public string GetValue()
    {
        return value;
    }

    void OnValidate()
    {
        if(string.IsNullOrEmpty(value))
            value = name;
    }
}

[CreateAssetMenu(menuName = "Dice/Advanced/Side/Image Side")]
public class DiceImageSideConfig : DiceSideAdvanced, IDiceSideViewInfo
{
    public Sprite image;

    public object GetRepresentation()
    {
        return image;
    }
}

