﻿public interface IDiceSide: WithProbability
{
    string GetValue();
}