﻿using UnityEngine;

public abstract class IDice<T> : ScriptableObject
{
    public DiceView dice;

    public abstract T GetResult();
}