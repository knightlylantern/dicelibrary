﻿
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dice/Dice Number")]
public class Dice :  IDice<int>
{
    public int sides;

    public override int GetResult()
    {
        return Random.Range(1, sides+1);
    }
}


public class Dice<T> : IDice<T>
{
    public List<T> sides;

    public override T GetResult()
    {
        float probSide = 1f / sides.Count;
        float prob = UnityEngine.Random.value;

        return sides[(int)(prob / probSide)];
    }
}


