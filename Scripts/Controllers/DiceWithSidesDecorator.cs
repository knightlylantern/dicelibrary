﻿using System;
using RSG;
using UnityEngine;

public class DiceWithSidesDecorator<T, T2>: DiceDecorator<T> where T : DiceSide<T2>
{
    public DiceWithSidesDecorator(IDice<T> dice, Vector3 position) : base(dice, position)
    {
    }

    public DiceWithSidesDecorator(IDice<T> dice, Transform parent = null) : base(dice, parent)
    {
    }

    public IPromise<T2> RollValue()
    {
        return Roll().Then(result =>
        {
            return Promise<T2>.Resolved(result.value);
        });
    }

}


