﻿using System;
using RSG;
using UnityEngine;

public class DiceAdvancedDecorator
{

    DiceAdvanced dice;
    DiceView view;
    private Vector3 vector3;

    public DiceAdvancedDecorator(DiceAdvanced dice, Vector3 position) : this(dice)
    {
        view.transform.position = position;
    }

   

    public DiceAdvancedDecorator(DiceAdvanced dice, Transform parent = null)
    {
        this.dice = dice;
        this.view = GameObject.Instantiate(dice.dice, parent);
    }



    public IPromise<string> Roll()
    {
        var result = dice.GetResult();

        return view.RollView().Then(()=>
        {
            if(result is IDiceSideViewInfo)
                view.ShowResult(((IDiceSideViewInfo)result).GetRepresentation());

            else
                view.ShowResult(result);

            return Promise<string>.Resolved(result.GetValue());
        });
    }

}


