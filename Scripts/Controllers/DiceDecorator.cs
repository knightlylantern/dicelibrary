﻿using System;
using RSG;
using UnityEngine;

public class DiceDecorator<T> 
{

    IDice<T> dice;
    DiceView view;

    public DiceDecorator(IDice<T> dice, Vector3 position) : this(dice)
    {
        view.transform.position = position;
    }

   

    public DiceDecorator(IDice<T> dice, Transform parent = null)
    {
        this.dice = dice;
        this.view = GameObject.Instantiate(dice.dice, parent);
    }


    public IPromise<T> Roll()
    {
        var result = dice.GetResult();

        return view.RollView().Then(()=>
        {
            view.ShowResult(result);
            return Promise<T>.Resolved(result);
        });
    }

    public void Hide()
    {
        view.gameObject.SetActive(false);
    }
}


